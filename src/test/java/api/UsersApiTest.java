package api;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

import static io.restassured.RestAssured.given;

public class UsersApiTest {

    private int callsNum;
    private int threadNum;
    private RequestSpecification requestSpecification;
    private SoftAssert soft;
    private JSONObject requestParams;

    @BeforeClass
    public void setup() {
        callsNum = 100;
        threadNum = 10;
        soft = new SoftAssert();
        requestSpecification = new RequestSpecBuilder()
                .setContentType(ContentType.JSON)
                .setBaseUri("https://reqres.in")
                .setBasePath("api/users")
                .build();

        requestParams = new JSONObject();
        requestParams.put("first_name", "John");
        requestParams.put("last_name", "Doe");
    }

    @Test
    public void testGetUser() throws InterruptedException {
        AtomicInteger index = new AtomicInteger();
        final ExecutorService executor = Executors.newFixedThreadPool(threadNum);
        final CountDownLatch latch = new CountDownLatch(callsNum);
        List<Response> responses = new ArrayList<>();
        IntStream.range(0, (int) latch.getCount())
                .forEach(i -> {
                            Runnable task = () -> {
                                try {
                                    responses.add(given(requestSpecification)
                                            .get().then().log().ifValidationFails().statusCode(HttpStatus.SC_OK)
                                            .extract().response());
                                } finally {
                                    latch.countDown();
                                }
                            };
                            executor.submit(task);
                        }
                );
        latch.await();
        executor.shutdown();

        responses.forEach(response ->
                soft.assertEquals(
                        response.getStatusCode(), HttpStatus.SC_OK, "Response num:" + (index.getAndIncrement())
                )
        );
        soft.assertAll();
    }

    @Test
    public void testPostUser() throws InterruptedException {
        AtomicInteger index = new AtomicInteger();
        final ExecutorService executor = Executors.newFixedThreadPool(threadNum);
        final CountDownLatch latch = new CountDownLatch(callsNum);
        List<Response> responses = new ArrayList<>();
        IntStream.range(0, (int) latch.getCount())
                .forEach(i -> {
                            Runnable task = () -> {
                                try {
                                    responses.add(given(requestSpecification)
                                            .body(requestParams.toString())
                                            .post().then().log().ifValidationFails().statusCode(HttpStatus.SC_CREATED)
                                            .extract().response());
                                } finally {
                                    latch.countDown();
                                }
                            };
                            executor.submit(task);
                        }
                );
        latch.await();
        executor.shutdown();

        responses.forEach(response -> {
                    int count = index.getAndIncrement();
                    soft.assertEquals(response.getStatusCode(), HttpStatus.SC_CREATED, "Response:" + count);
                    soft.assertEquals(response.getBody().path("first_name"), "John", "Response:" + count);
                    soft.assertEquals(response.getBody().path("last_name"), "Doe", "Response:" + count);
                }
        );
        soft.assertAll();
    }
}
