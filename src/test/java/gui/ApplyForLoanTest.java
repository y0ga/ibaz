package gui;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static org.testng.Assert.assertFalse;

public class ApplyForLoanTest {

    private String form_inputs = "//div[@class='input-container']//input";
    private List<String> list;
    private AtomicInteger index;
    private SoftAssert soft;

    @BeforeClass
    public void setUp() {
        open("https://ibar.az/en/ferdi/tecili-kredit-sifarisi");
    }

    @BeforeMethod
    public void createCollection() {
        soft = new SoftAssert();
        list = new ArrayList<>();
        index = new AtomicInteger();
    }

    @AfterMethod
    public void assertAll() {
        soft.assertAll();
    }

    @Test(priority = 1)
    public void testValidationNotPresent() {
        $$(byXpath(form_inputs)).forEach(el -> list.add(el.getAttribute("class")));
        list.forEach(s -> soft.assertFalse(s.contains("parsley-error"), "Validation is present for input №" + index.getAndIncrement()));
    }

    @Test(priority = 2)
    public void testValidationPresentInAllInputs() {
        $(byText("Send")).click();
        $$(byXpath(form_inputs)).forEach(el -> list.add(el.getAttribute("class")));
        list.forEach(s -> soft.assertTrue(s.contains("parsley-error"), "Validation is present for input №" + index.getAndIncrement()));
    }

    @Test(priority = 3)
    public void testValidationNotPresentInName() {
        $$(byXpath(form_inputs)).get(0).setValue("John Doe");
        String class_value = $$(byXpath(form_inputs)).get(0).getAttribute("class");
        assertFalse(class_value.contains("parsley-error"), "Validation is present in Name");
    }

    @Test(priority = 4)
    public void testValidationNotPresentInId() {
        $$(byXpath(form_inputs)).get(1).setValue("1234567");
        String class_value = $$(byXpath(form_inputs)).get(1).getAttribute("class");
        assertFalse(class_value.contains("parsley-error"), "Validation is present ID");
    }

    @Test(priority = 5)
    public void testValidationNotPresentInPhone() {
        $$(byXpath(form_inputs)).get(2).sendKeys("9999999999");
        String class_value = $$(byXpath(form_inputs)).get(2).getAttribute("class");
        assertFalse(class_value.contains("parsley-error"), "Validation is present Phone");
    }

    @Test(priority = 6)
    public void testValidationNotPresentForFilledInputs() {
        $$(byXpath(form_inputs)).stream().limit(3).forEach(el -> list.add(el.getAttribute("class")));
        list.forEach(s -> soft.assertFalse(s.contains("parsley-error"), "Validation is present for input №" + index.getAndIncrement()));
    }
}
