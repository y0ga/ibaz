package gui;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.open;

public class AmExPresenceTest {

    private SoftAssert soft = new SoftAssert();
    private String card_labels = "//div[@class='pt-4']//h4";

    @BeforeClass
    public void setUp() {
        open("https://ibar.az/en/ferdi/kartlar/status-kartlari");
    }

    @Test(priority = 1)
    public void testLanguageSwitchToEn() {
        List<String> list = $$(byXpath(card_labels)).texts();
        soft.assertTrue(list.contains("American Express Green"), "Expected card not present on page");
        soft.assertTrue(list.contains("American Express Gold"), "Expected card not present on page");
        soft.assertAll();
    }
}
