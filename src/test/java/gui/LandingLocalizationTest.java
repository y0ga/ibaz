package gui;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.testng.Assert.assertTrue;

public class LandingLocalizationTest {

    private String change_language = "//div[@class='container-fluid']//div[@class='selectric']//span[text()='%s']";
    private String options_language = "//div[@class='container-fluid']//div[@class='selectric-scroll']//li[text()='%s']";

    @BeforeClass
    public void setUp() {
        open("https://ibar.az/");
    }

    @Test(priority = 1)
    public void testLanguageSwitchToEn() {
        $(byXpath(String.format(change_language, "AZ"))).click();
        $(byXpath(String.format(options_language, "EN"))).click();
        assertTrue(url().contains("en"), "Url not contains expected language");
    }

    @Test(priority = 2)
    public void testLanguageSwitchToRu() {
        $(byXpath(String.format(change_language, "EN"))).click();
        $(byXpath(String.format(options_language, "RU"))).click();
        assertTrue(url().contains("ru"), "Url not contains expected language");
    }

    @Test(priority = 3)
    public void testLanguageSwitchToAz() {
        $(byXpath(String.format(change_language, "RU"))).click();
        $(byXpath(String.format(options_language, "AZ"))).click();
        assertTrue(url().contains("az"), "Url not contains expected language");
    }

}
