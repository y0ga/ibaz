# BDD Task.

-- Analyse the IBAR website and design some scenarios that you would test on the website:  
-- Describe these scenarios using Behaviour Driven Development (BDD) / Keyword(Action) Driven Testing  
-- Describe at least 5 unrelated scenario  

# Feature: Internet Banking

## Scenario: Private banking login options

**Given:** User is on ibar.az main page  
**And:** "EN" language is set  
**When:** User clicks on "Internet Banking"  
**And** Chooses "Personal Internet Banking"  
**Then:** "IBAbanking" option is present  
**And**  "Azericard Internet Banking" option is present  

## Scenario: Login with "IBAbanking" 

**Given:** User is on "Personal Internet Banking" page  
**When:** User clicks on "IBAbanking"  
**Then:** "IBAbanking" login page is opened on new tab  

## Scenario: "IBAbanking" login validation  

**Given:** User is on "IBAbanking" login page  
**When:** Types "John" in Login input  
**And**  Types "johnsecret" in Password input  
**Then:** "Invalid login" validation appears  
**Then:** "Invalid password" validation appears  


# Feature: Loan calculator

## Scenario: Loan calculator section is present

**Given:** User is on ibar.az main page  
**And:** "EN" language is set  
**When:** User scrolls down the page  
**Then:** Loan calculator section should be present  


## Scenario: Change loan inputs

**Given:** User is on Loan calculator section  
**When:** Types ""7500" in "Loan amount" input  
**And:** Set "Loan term" to 12 months  
**And:** Change "Interest rete" to 27%  
**Then:** Monthly payment is changed accordingly   
**And:** Total amount is changed accordingly  

## Scenario: Apply for a loan online

**Given:** User changed loan inputs to custom  
**When:** Clicks on "Apply online"  
**Then:** "Apply for a loan online" form is opened  

# Feature: Online support

## Scenario: Open online chat
**Given:** User is on ibar.az main page  
**And:** "EN" language is set  
**When:** User clicks on :speech_balloon: button in bottom right corner  
**Then:** Online chat dialog is present  
**And**  Chat language is "EN"  


# Feature: Currency rates

## Scenario: Currency rates section is present

**Given:** User is on ibar.az main page  
**And:** "EN" language is set  
**When:** User scrolls down the page  
**Then:** Currency section should be present  

## Scenario: Available currencys in "sell" field

**Given:** User is on Currency rates section  
**When:** User clicks on dropdown in "I would like to sell" field  
**Then:** Followint currency options should appear:   

| USD |  
|-----|  
| AZN |  
| RUB |  
| EUR |   

## Scenario: Available currencys in "get" field

**Given:** User is on Currency rates section  
**When:** User clicks on dropdown in "I will get" field  
**Then:** Followint currency options should appear:  

| AZN |  
|-----|  
| USD |  
| RUB |  
| EUR |  


## Scenario: Currency calculator conversion

**Given:** User is on Currency calculator section  
**When:** User set "I will get" value to "1"  
**And:** Sets currency to "EUR"  
**Then:** "I will get" value is changed accordingly to EUR/AZN rate (≈1.9)  